package oocl.afs.todolist.controller;

import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoResponse;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    List<TodoResponse> getAll() {
        return todoService.findAll();
    }

    @PostMapping
    TodoResponse create(){
        return null;
    }

    @PutMapping
    TodoResponse update(){
        return null;
    }

    @DeleteMapping
    boolean delete(){
        return false;
    }

}
